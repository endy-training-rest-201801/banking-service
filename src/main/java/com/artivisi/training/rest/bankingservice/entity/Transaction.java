package com.artivisi.training.rest.bankingservice.entity;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Min;

import org.hibernate.annotations.GenericGenerator;

import lombok.Data;

@Entity @Table(name="account_transaction")
@Data
public class Transaction {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @JoinColumn(name = "id_account")
    private Account account;

    @Enumerated(EnumType.STRING)
    private TransactionType transactionType;

    @Min(0)
    private BigDecimal amount = BigDecimal.ZERO;

    private LocalDateTime transactionTime;
}
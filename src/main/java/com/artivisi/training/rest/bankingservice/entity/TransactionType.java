package com.artivisi.training.rest.bankingservice.entity;

public enum TransactionType {
    WITHDRAW, DEPOSIT, TRANSFER_IN, TRANSFER_OUT, PURCHASE, PAYMENT
}
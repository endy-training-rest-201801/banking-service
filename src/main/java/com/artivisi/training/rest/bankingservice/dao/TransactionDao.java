package com.artivisi.training.rest.bankingservice.dao;

import com.artivisi.training.rest.bankingservice.entity.Transaction;

import org.springframework.data.repository.PagingAndSortingRepository;

public interface TransactionDao extends PagingAndSortingRepository<Transaction, String> {

}
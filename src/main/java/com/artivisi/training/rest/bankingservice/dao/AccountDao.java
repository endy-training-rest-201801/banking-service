package com.artivisi.training.rest.bankingservice.dao;

import com.artivisi.training.rest.bankingservice.entity.Account;

import org.springframework.data.repository.PagingAndSortingRepository;

public interface AccountDao extends PagingAndSortingRepository<Account, String>{

}
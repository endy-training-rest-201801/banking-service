package com.artivisi.training.rest.bankingservice.dao;

import com.artivisi.training.rest.bankingservice.entity.Customer;

import org.springframework.data.repository.PagingAndSortingRepository;

public interface CustomerDao extends PagingAndSortingRepository<Customer, String> {

}
package com.artivisi.training.rest.bankingservice.hateoas;

import com.artivisi.training.rest.bankingservice.controller.CustomerController;
import com.artivisi.training.rest.bankingservice.entity.Customer;

import org.springframework.hateoas.ResourceSupport;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;


import lombok.Getter;

@Getter
public class CustomerResource extends ResourceSupport {
    private final Customer customer;

    public CustomerResource(Customer c){
        this.customer = c;
        add(linkTo(CustomerController.class).withRel("customers"));
    }
}
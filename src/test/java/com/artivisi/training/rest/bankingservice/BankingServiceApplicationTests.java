package com.artivisi.training.rest.bankingservice;

import com.artivisi.training.rest.bankingservice.dao.CustomerDao;
import com.artivisi.training.rest.bankingservice.entity.Customer;
import com.github.javafaker.Faker;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BankingServiceApplicationTests {

	@Autowired private CustomerDao customerDao;

	@Test
	public void insertSampleData() {
		Faker faker = new Faker();
		for(int i = 0; i<50; i++) {
			String firstname = faker.name().firstName();
			String lastname = faker.name().lastName();
			String domain = faker.internet().domainName();
			Customer customer = new Customer();
			customer.setCustomerNumber(faker.idNumber().valid());
			customer.setName(firstname + " " + lastname);
			customer.setEmail(firstname+"@"+domain);
			customer.setMobilePhone(faker.phoneNumber().cellPhone());
			customerDao.save(customer);
		}

		Assert.assertTrue(customerDao.count() > 0);
	}

}
